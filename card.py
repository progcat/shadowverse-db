import urllib3
import sqlite3
import json
import db
import os

PORTAL_URL = 'https://shadowverse-portal.com'
PORTAL_API = PORTAL_URL + '/api/v1/cards/'
PORTAL_IMAGE = PORTAL_URL + '/image/card/en/'


def getClanCards():
    http = urllib3.PoolManager()
    res = http.request('GET', PORTAL_API + '?format=json&lang=en')
    if res.status != 200:
        print('[getClanCards] Cannot get card data from server! status:', res.status)
        return None
    card_data = (json.loads(res.data.decode('utf-8'))['data'])['cards']
    return card_data if len(card_data) != 0 else None

def saveCardImage(dir, ID, evolved=False):
    if type(ID) != int:
        print('[getCardImage] Unknown ID!')
        return
    path = dir + '/' + str(ID) + '.png'
    if os.path.exists(path):
        return path
    base_url = PORTAL_IMAGE + 'C_' + str(ID) + '.png'
    evolved_url = PORTAL_IMAGE + 'E_' + str(ID) + '.png'
    http = urllib3.PoolManager()
    if evolved:
        print('Downloading ', path, 'from', evolved_url)
        res = http.request('GET', evolved_url)
    else:
        print('Downloading ', path, 'from', base_url)
        res = http.request('GET', base_url)
    if res.status != 200:
            print('[getCardImage] Cannot get image data from server! status:', res.status)
            return None
    
    f = open(path, 'wb')
    f.write(res.data)
    f.close()
    return path

if __name__ == '__main__':
    #NOTE: some how clan=0 return all cards

    #pictures are sorted by card id.
    #evolved card has same id as its original one,
    #so need seprate directory to store them
    image_path = 'img'
    evo_image_path = image_path + '/evolved'


    characters = ['neutral', 'arisa', 'erika', 'isabelle', 'rowen', 'luna', 'urias', 'eris', 'yuwan']
    conn = db.init('cards.db')
    #get card data from server
    data = getClanCards()
    for i in range(0, 9):
        print('current clan:', characters[i])
        db.createTable(conn, characters[i])
        card_count = 0
        for d in data:
            base = saveCardImage('img', d['card_id'])
            evo = saveCardImage('img/evolved', d['card_id'], True)
            #if current card not in current clan
            if d['clan'] != i:
                #if yes, skip.
                continue
            db.insert(conn, characters[i], d, base, evo)
            card_count += 1
        print(card_count, 'cards added')
    conn.close()