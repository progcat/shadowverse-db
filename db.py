import sqlite3

def init(db_file):
    con = sqlite3.connect(db_file)
    return con

def createTable(db, table):
    if type(db) != sqlite3.Connection:
        print('Invalid connection')
        return
    cur = db.cursor()

    cur.execute('CREATE TABLE ' + str(table) + '(\
    card_id INT PRIMARY KEY NOT NULL, \
    card_name TEXT NOT NULL, \
    card_type INT NOT NULL, \
    clan INT NOT NULL,\
    trait TEXT NOT NULL,\
    skill_des TEXT NOT NULL,\
    evo_skill_des TEXT NOT NULL,\
    cost INT NOT NULL,\
    attack INT NOT NULL,\
    life INT NOT NULL,\
    evo_attack INT NOT NULL,\
    evo_life INT NOT NULL,\
    rarity INT NOT NULL,\
    vials_gain INT NOT NULL,\
    vials_need INT NOT NULL,\
    base_image TEXT NOT NULL,\
    evo_image TEXT NOT NULL\
    );')
    
def insert(db, table, data, base, evo):
    cmd = 'INSERT INTO ' + table + \
    ' VALUES('   + str(data['card_id']) + ', "'\
                 + data['card_name']+ '", '\
                 + str(data['char_type'])+ ', '\
                 + str(data['clan'])+ ', "'\
                 + data['tribe_name']+ '", "'\
                 + data['skill_disc']+ '", "'\
                 + data['evo_skill_disc']+ '", '\
                 + str(data['cost'])+ ', '\
                 + str(data['atk'])+ ', '\
                 + str(data['life'])+ ', '\
                 + str(data['evo_atk'])+ ', '\
                 + str(data['evo_life'])+ ', '\
                 + str(data['rarity'])+ ', '\
                 + str(data['get_red_ether'])+ ', '\
                 + str(data['use_red_ether'])+ ', "'\
                 + base+ '", "'\
                 + evo\
                 + '");'
    db.execute(cmd)
    db.commit()